+++
title = "About"
+++

## About Me

{{< figure class="avatar" src="/avatar.jpg" alt="avatar">}}

I am currently a  developer researcher at [Xilam](https://xilam.com/en/) and an associate researcher at the [INREV](https://inrev.univ-paris8.fr/) research team.

## Research Interest

My research questions the contribution of real-time rendering technologies in the manufacturing process of animated films and explores creative methods that improve the communication of the artistic group through the design and experimentation of real-time collaboration tools.

## Publications

In chronological order:
1. Swann Martinez, Chu-Yin Chen, Jean-Claude Hoyami. Une nouvelle frontière numérique pour la cocréation 3D. Frontières Numériques 2023, Imad Saleh Samuel Szoniecky, Malek Ghenima E, Jun 2023, Hammamet, Tunisie. ⟨hal-04429105⟩
2. Swann Martinez. Collaboration en temps réel pour la production de films d’animation numérique. Multimédia [cs.MM]. Université Paris 8, 2022. Français. ⟨[tel-04018090](https://hal.science/tel-04018090v1)⟩
3. S. Martinez and C.-Y. Chen, “A Framework Enabling Real-time Multi-user Collaborative Workflow in 3D Digital Content Creation Software” presented at the WSCG, 2021. doi: [10.24132/CSRN.2021.3101.10](https://otik.uk.zcu.cz/handle/11025/45013).
4. S. Martinez and C.-Y. Chen, “Être en apesanteur : Une approche diégétique en réalité virtuelle,” Entrelacs. Cinéma et audiovisuel, no. 17, Art. no. 17, Jun. 2020, doi: [10.4000/entrelacs.5918](https://doi.org/10.4000/entrelacs.5918).