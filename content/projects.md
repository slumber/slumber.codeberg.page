+++
title = "Projects"
+++

## Projects

Most of my projects can be found on my [GitLab](https://gitlab.com/slumber)
page.

* [**Multi-User**](https://gitlab.com/slumber/multi-user) : an open source Blender add-on that allows multiple artists to work in real time on the same 3D scene via the network.

* [**Smartphone Remote**](https://gitlab.com/slumber/smartphoneremote) : an open source Blender add-on to bring wireless spatialized controller for the masses. 