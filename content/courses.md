+++
title = "Slides"
+++

# Slides


- [A short Reveal.Js introduction](/slides/revealjs-introduction/index.html) - [[Sources](https://codeberg.org/slumber/revealjs-introduction)]
- Organizational tips for the thesis - [French] [[Sources](https://codeberg.org/slumber/phd-pipeline-course-materials)]
  - [PRAXIS 2019](/slides/phd-pipeline-course/2019/PhD_Pipeline.pdf)
    - Gérer son activité de veille
    - Versionner son travail 
    - Rythmer son travail en sprint
    - Accumuler des données
  - [PRAXIS 2020](/slides/phd-pipeline-course/2020/index.html)
    - Structurer les connaissances accumulées (Zettlekasten)
    - Présenter son travail avec RevealJS, un framework opensource
  - [PRAXIS 2021](/slides/phd-pipeline-course/2021/index.html)
    - Retour sur les ateliers précedents
    - Workflow pour rédiger sa thèse en Latex
      - Utiliser Overleaf pour la review
      - Utiliser VScode pour rédiger (avec les plugins qui vont bien pour traiter tout les aspects de la rédaction)
      - Sauvegarder sont manuscripts avec Syncthings
- Production pipelines - [French] [[Sources](https://codeberg.org/slumber/pipeline)]
  1. [Organisation du cours](/slides/production-pipeline/?course-module=module_0-course_introduction)
  2. [Qu'est-ce qu'un pipeline](/slides/production-pipeline/?course-module=module_1-introduction_generale) [[Notes](/slides/production-pipeline/?course-module=module_1-introduction_generale&show-note)]
  3. [Pipeline pour le pré calculé](/slides/production-pipeline/?course-module=module_2-pipeline_pour_le_film_animation) [[Notes](/slides/production-pipeline/?course-module=module_2-pipeline_pour_le_film_animation&show-note)]
  1. [Pipeline pour le temps réel](/slides/production-pipeline/?course-module=module_3-pipeline_pour_le_jeu_video) [[Notes](/slides/production-pipeline/?course-module=module_3-pipeline_pour_le_jeu_video&show-note)]
  2. [Méthode agiles](/slides/production-pipeline/?course-module=module_4-methodes_agiles) [[Notes](/slides/production-pipeline/?course-module=module_4-methodes_agiles&show-note)]
  3. [Collaboration en temps réel](/slides/production-pipeline/?course-module=module_5-collaboration_en_temps_reel) [[Notes](/slides/production-pipeline/?course-module=module_5-collaboration_en_temps_reel&show-note)]
  4. [TP: Kapla](/slides/production-pipeline/?course-module=module_5-TP0-kapla) [[Notes](/slides/production-pipeline/?course-module=module_5-TP0-kapla&show-note)]